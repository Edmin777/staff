<?php

require 'connect_db.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>staff</title>
</head>
<body>
    <div class="table">
        <h1>Отдел кадров</h1>
        <div class="links">
        <a href="index.php" class="link">Все</a>
        <a href="zap3.php" class="link">Начальники</a>
        <a href="zap2.php" class="link">Испыт. срок</a>
        <a href="zap1.php" class="link">Уволенные</a>
        </div>
    <table id="table">
        <tr>
            <th>id</th>
            <th>ФИО</th>
            <th>Дата рождения</th>
            <th>created_at</th>
            <th>update_at</th>
        </tr>

        <?php
            
            $query = $pdo->query('SELECT * FROM `user`');
            while($row = $query->fetch(PDO::FETCH_OBJ)){
                ?>
                <tr>
                    <td><?= $row->id ?></td>
                    <td><?= ' ' . $row->last_name . ' ' . $row->first_name . ' ' . $row->middle_name ?></td>
                    <td><?= $row->data_of_birth ?></td>
                    <td><?= $row->created_at ?></td>
                    <td><?= $row->update_at ?></td>
                </tr>
            <?php
            };
    
            ?>
        </table>
      
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

    <script>

        $(document).ready(function() {

            $('.link').click(function(){

            var toLoad = $(this).attr('href')+' #table';
            $('#table').hide(0 ,loadContent);
            function loadContent() {
            $('#table').load(toLoad, 0,showNewContent())
            }
            function showNewContent() {
            $('#table').show(0);
            }

            return false;

            });
            });
    </script>
</body>
</html>