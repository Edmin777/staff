<?php

require 'connect_db.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>staff</title>
</head>
<body>
    <div class="table">
        <h1>Отдел кадров</h1>
        <div class="links">
        <a href="index.php" class="link">Все</a>
        <a href="zap1.php" class="link">Испыт. срок</a>
        <a href="#" class="link">Уволенные</a>
        </div>
    <table id="table">
        <tr>
            <th>ФИО</th>
            <th>Причина увольнения</th>
        </tr>

        <?php

            $query = $pdo->query('SELECT user.first_name, user.last_name, dismission_reason.description FROM user_dismission
            LEFT JOIN user ON (user.id = user_dismission.user_id)
            LEFT JOIN dismission_reason ON (dismission_reason.id = user_dismission.reason_id)');
            while($row = $query->fetch(PDO::FETCH_OBJ)){
                ?>
                <tr>
                    <td><?= ' ' . $row->last_name . ' ' . $row->first_name ?></td>
                    <td><?= $row->description ?></td>
                </tr>
            <?php
            };
            ?>
        </table>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</body>
</html>
