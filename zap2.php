<?php

require 'connect_db.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>staff</title>
</head>
<body>
    <div class="table">
        <h1>Отдел кадров</h1>
        <div class="links">
        <a href="index.php" class="link">Все</a>
        <a href="zap1.php" class="link">Испыт. срок</a>
        <a href="#" class="link">Уволенные</a>
        </div>
    <table id="table">
        <tr>
            <th>id</th>
            <th>ФИО</th>
            <th>Дата рождения</th>
            <th>Дата приема на работу</th>
            <th>update_at</th>
        </tr>

        <?php

            $query = $pdo->query('SELECT * FROM `user` WHERE `created_at` BETWEEN CURDATE() - INTERVAL 3 MONTH AND CURDATE() ORDER BY `last_name` ASC');
            while($row = $query->fetch(PDO::FETCH_OBJ)){
                ?>
                <tr>
                    <td><?= $row->id ?></td>
                    <td><?= ' ' . $row->last_name . ' ' . $row->first_name . ' ' . $row->middle_name ?></td>
                    <td><?= $row->data_of_birth ?></td>
                    <td><?= $row->created_at ?></td>
                    <td><?= $row->update_at ?></td>
                </tr>
            <?php
            };
            ?>
        </table>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</body>
</html>