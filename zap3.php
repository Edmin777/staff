<?php

require 'connect_db.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>staff</title>
</head>
<body>
    <div class="table">
        <h1>Отдел кадров</h1>
        <div class="links">
        <a href="index.php" class="link">Все</a>
        <a href="zap1.php" class="link">Испыт. срок</a>
        <a href="#" class="link">Уволенные</a>
        </div>
    <h2>Начальники</h2>
    <table id="table">
        <tr>
            <th>Начальник</th>
            <th>Отдел</th>
        </tr>

        <?php

            $query = $pdo->query('SELECT user.first_name, user.last_name, department.description FROM user_position LEFT JOIN user ON (user.id = user_position.user_id) LEFT JOIN department ON (user.id = department.leader_id) WHERE description IS NOT NULL');
            while($row = $query->fetch(PDO::FETCH_OBJ)){
                ?>
                <tr>
                    <td><?= ' ' . $row->last_name . ' ' . $row->first_name . ' ' . $row->middle_name ?></td>
                    <td><?= $row->description ?></td>
                </tr>
            <?php
            };
            ?>
        </table>

        <table id="table">
        <tbody>Сотрудники</tbody>
        <tr>
            <th>Сотрудник</th>
            <th>Отдел</th>
        </tr>

        <?php

            $query = $pdo->query('SELECT user.first_name, user.last_name, department.description FROM user_position LEFT JOIN user ON (user.id = user_position.user_id) LEFT JOIN department ON (department.id = user_position.department_id)');
            while($row = $query->fetch(PDO::FETCH_OBJ)){
                ?>
                <tr>
                    <td><?= ' ' . $row->last_name . ' ' . $row->first_name . ' ' . $row->middle_name ?></td>
                    <td><?= $row->description ?></td>
                </tr>
            <?php
            };
            ?>

    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</body>
</html>